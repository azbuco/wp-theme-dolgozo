<?php
cc_debug('Template: ' . __FILE__);
get_header();
?>

<div class="content-area">
    <div class="container">
        <?php while (have_posts()) : the_post(); ?> 
            <div class="row">
                <div class="col-md-12">
                    <main id="main" role="main">
                        <?php the_content() ?>
                    </main>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
</div>

<?php
get_footer();
