<?php
cc_debug('Template: ' . __FILE__);
get_header();
?>
<div class="content-area">
    <div class="container">

        <?php while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-md-8">
                    <?php the_title('<h1 class="main-title mb-0">', '</h1>'); ?>
                    <div class="post-meta mt-1 mb-5 text-muted d-flex flex-wrap align-items-center">
                        <span class="post-date mr-3 text-nowrap"><?php the_time(get_option('date_format') . ' ' . get_option('time_format')) ?></span>

                        <span class="post-categories d-flex mr-2">
                            <?php echo get_template_part('template-parts/meta-categories') ?>
                        </span>
                        
                        <span class="post-tags d-flex">
                            <?php echo get_template_part('template-parts/meta-tags') ?>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <main id="main" role="main" class="mb-5">
                        <?php the_content() ?>
                    </main>
                </div>
                <div class="col-md-4 pl-md-5">
                    <aside>
                        <?php if (is_active_sidebar('sidebar-default')) : ?>
                            <?php dynamic_sidebar('sidebar-default'); ?>
                        <?php endif; ?>
                    </aside>
                </div>
            </div>

        <?php endwhile; ?>
    </div>
</div>

<?php get_footer(); ?>
