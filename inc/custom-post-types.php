<?php
/*
 * Theme related post types
 */

$cpt_path = dirname(__FILE__) . '/cpt';
foreach(glob($cpt_path . '/*', GLOB_ONLYDIR) as $folder) {
    foreach(glob($folder . '/[!_]*.php') as $file) {
        require_once $file;
    }
}

