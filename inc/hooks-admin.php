<?php
/*
 * Hooks for the admin interface
 */

/**
 * Admin menu items visibility
 */
function cc_admin_menu_disable_menus()
{
//    remove_menu_page('edit-comments.php');
//    remove_menu_page('link-manager.php');
//    remove_menu_page( 'tools.php' );
//    remove_menu_page( 'plugins.php' );
//    remove_menu_page( 'users.php' );
//    remove_menu_page( 'options-general.php' );
//    remove_menu_page( 'upload.php' );
//    remove_menu_page('edit.php');
//    remove_menu_page('edit.php?post_type=page');
//    remove_menu_page( 'themes.php' );
}
add_action('admin_menu', 'cc_admin_menu_disable_menus', 99, 0);

function cc_admin_menu_disable_submenus()
{
//    remove_submenu_page('themes.php', 'theme-editor.php');
//    remove_submenu_page('themes.php', 'themes.php');
//    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
//    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
//    remove_submenu_page('edit.php', 'post-new.php');
//    remove_submenu_page('themes.php', 'nav-menus.php');
//    remove_submenu_page('themes.php', 'widgets.php');
//    remove_submenu_page('themes.php', 'theme-editor.php');
//    remove_submenu_page('plugins.php', 'plugin-editor.php');
//    remove_submenu_page('plugins.php', 'plugin-install.php');
//    remove_submenu_page('users.php', 'users.php');
//    remove_submenu_page('users.php', 'user-new.php');
//    remove_submenu_page('upload.php', 'media-new.php');
//    remove_submenu_page('options-general.php', 'options-writing.php');
//    remove_submenu_page('options-general.php', 'options-discussion.php');
//    remove_submenu_page('options-general.php', 'options-reading.php');
//    remove_submenu_page('options-general.php', 'options-discussion.php');
//    remove_submenu_page('options-general.php', 'options-media.php');
//    remove_submenu_page('options-general.php', 'options-privacy.php');
//    remove_submenu_page('options-general.php', 'options-permalinks.php');
//    remove_submenu_page('index.php', 'update-core.php');
}
add_action('admin_menu', 'cc_admin_menu_disable_submenus', 99, 0);

function cc_admin_bar_disable_items()
{
    global $wp_admin_bar;
//    $wp_admin_bar->remove_menu('user-actions');
//    $wp_admin_bar->remove_menu('user-info');
//    $wp_admin_bar->remove_menu('edit-profile');
//    $wp_admin_bar->remove_menu('logout');
//    $wp_admin_bar->remove_menu('menu-toggle');
//    $wp_admin_bar->remove_menu('my-account');
//    $wp_admin_bar->remove_menu('wp-logo');
//    $wp_admin_bar->remove_menu('about');
//    $wp_admin_bar->remove_menu('wp-org');
//    $wp_admin_bar->remove_menu('documentation');
//    $wp_admin_bar->remove_menu('support-forums');
//    $wp_admin_bar->remove_menu('feedback');
//    $wp_admin_bar->remove_menu('site-name');
//    $wp_admin_bar->remove_menu('view-site');
//    $wp_admin_bar->remove_menu('comments');
//    $wp_admin_bar->remove_menu('new-content');
//    $wp_admin_bar->remove_menu('new-post');
//    $wp_admin_bar->remove_menu('new-media');
//    $wp_admin_bar->remove_menu('new-page');
//    $wp_admin_bar->remove_menu('new-user');
//    $wp_admin_bar->remove_menu('archive');
//    $wp_admin_bar->remove_menu('top-secondary');
//    $wp_admin_bar->remove_menu('wp-logo-external');
}
add_action('wp_before_admin_bar_render', 'cc_admin_bar_disable_items', 99, 0);

add_filter('custom_menu_order', '__return_true');

function cc_menu_order($array)
{
    return array(
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php?post_type=page', // Pages
    );
}
add_filter('menu_order', 'cc_menu_order', 10, 1);

//function cc_admin_menu_rename_posts()
//{
//    global $menu;
//    $menu[5][0] = 'Blog Posts';
//}
//add_action('admin_menu', 'cc_admin_menu_rename_posts');


/**
 * Editor css
 */
add_editor_style('css/editor-style.css');

/**
 * Tinymce configuration
 */
 /*
function cc_tiny_mce_remove_styleselect_button($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'cc_tiny_mce_remove_styleselect_button', 10, 1);

function cc_tiny_mce_custom_styles($init_array)
{
    // disable object resize
    $init_array['object_resizing'] = false;

    // table default classes
    $init_array['table_default_attributes'] = "{class: 'table table-condensed table-striped'}";

    // table classes
    $init_array['table_class_list'] = "[{title: 'None', value: 'table table-condensed'}, {title: 'Sávos', value: 'table table-condensed table-striped'}]";

    $init_array['style_formats'] = json_encode(array(
        // Define the style_formats array
        // Each array child is a format with it's own settings
//        array(
//            'title' => 'Bevezető',
//            'block' => 'div',
//            'classes' => 'lead',
//            'wrapper' => true,
//        ),
//        array(
//            'title' => 'Kiemelés',
//            'block' => 'div',
//            'classes' => 'highlight',
//            'wrapper' => true,
//        ),
    ));

    return $init_array;
}
add_filter('tiny_mce_before_init', 'cc_tiny_mce_custom_styles', 10, 1);
*/

/**
 *  Replace category description with wsiwyg editor
 */
remove_filter('pre_term_description', 'wp_filter_kses');
remove_filter('term_description', 'wp_kses_data');

function cc_remove_default_category_description()
{
    global $current_screen;
    if ($current_screen->id == 'edit-category') {
        ?>
        <script type="text/javascript">
            jQuery(function ($) {
                $('textarea#description').closest('tr.form-field').remove();
            });
        </script>
        <?php
    }
}
add_action('admin_head', 'cc_remove_default_category_description');

function cc_wsiwg_category_description($tag)
{
    ?>
    <table class="form-table">
        <tr class="form-field">
            <th scope="row" valign="top"><label for="description"><?php _ex('Description', 'Taxonomy Description'); ?></label></th>
            <td>
                <?php
                $settings = array('wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name' => 'description');
                wp_editor(wp_kses_post($tag->description, ENT_QUOTES, 'UTF-8'), 'cat_description', $settings);
                ?>
                <br />
                <span class="description"><?php _e('The description is not prominent by default; however, some themes may show it.'); ?></span>
            </td>
        </tr>
    </table>
    <?php
}
add_filter('edit_category_form_fields', 'cc_wsiwg_category_description');

function cc_enable_svg_thumbnails()
{
    $css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';
    echo '<style type="text/css">' . $css . '</style>';
}
add_action('admin_head', 'cc_enable_svg_thumbnails');

/*
 * Filte post types in admin
 */
function cc_filter_by_taxonomy($post_type, $which)
{
    $types = array(
        'industry',
        'solution',
        'news'
    );

    if (!in_array($post_type, $types)) {
        return;
    }

    $taxonomy_slug = $post_type . '_category';

    // Retrieve taxonomy data
    $taxonomy_obj = get_taxonomy($taxonomy_slug);
    $taxonomy_name = $taxonomy_obj->labels->name;

    // Retrieve taxonomy terms
    $terms = get_terms($taxonomy_slug);

    // Display filter HTML
    echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
    echo '<option value="">' . sprintf(esc_html__('Show All %s', 'text_domain'), $taxonomy_name) . '</option>';
    foreach ($terms as $term) {
        printf(
            '<option value="%1$s" %2$s>%3$s (%4$s)</option>', $term->slug, ( ( isset($_GET[$taxonomy_slug]) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : ''), $term->name, $term->count
        );
    }
    echo '</select>';
}
add_action('restrict_manage_posts', 'cc_filter_by_taxonomy', 10, 2);

/*
 * Remove dashboard widgets
 */

function cc_remove_dashboard_meta()
{
    //global $wp_meta_boxes;

    remove_action('welcome_panel', 'wp_welcome_panel');
    
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal'); //Removes the 'incoming links' widget
    //remove_meta_box('dashboard_plugins', 'dashboard', 'normal'); //Removes the 'plugins' widget
    //remove_meta_box('dashboard_primary', 'dashboard', 'normal'); //Removes the 'WordPress News' widget
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal'); //Removes the secondary widget
    //remove_meta_box('dashboard_quick_press', 'dashboard', 'side'); //Removes the 'Quick Draft' widget
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side'); //Removes the 'Recent Drafts' widget
    //remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); //Removes the 'Activity' widget
    //remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); //Removes the 'At a Glance' widget
    remove_meta_box('dashboard_activity', 'dashboard', 'normal'); //Removes the 'Activity' widget (since 3.8)
    
    remove_meta_box('so-dashboard-news', 'dashboard', 'normal');
}
add_action('wp_dashboard_setup', 'cc_remove_dashboard_meta', 99);

