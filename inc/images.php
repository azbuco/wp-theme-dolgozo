<?php

/*
 * Register image sizes 
 */
set_post_thumbnail_size(1920, 9999, false);

// override defaults
add_image_size('medium', 600, 600, false);
add_image_size('large', 1240, 1240, false);

/*
 * Enable svg upload
 */
add_filter('upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});
