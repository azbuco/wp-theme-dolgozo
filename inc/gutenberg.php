<?php

// enable only for posts
function cc_enable_gutenberg_post_type($current_status, $post_type)
{
    if ($post_type !== 'post') {
        return false;
    }
    
    return $current_status;
}

add_filter('use_block_editor_for_post_type', 'cc_enable_gutenberg_post_type', 10, 2);
