<?php

/*
 * Reusable template functions
 */

/**
 * Apply basic formatting to text (like the_content filter) without messing other hooks
 */
add_filter('cc_theme_content', 'wptexturize');
add_filter('cc_theme_content', 'convert_smilies');
add_filter('cc_theme_content', 'convert_chars');
add_filter('cc_theme_content', 'wpautop');
add_filter('cc_theme_content', 'shortcode_unautop');
add_filter('cc_theme_content', 'prepend_attachment');

/**
 * Fejlesztoi információk megjelenítése csak localhoston noty.js segítségével
 * @param string $text
 * @param string $type
 * @param boolean $encode
 */
function cc_debug($text, $type = 'info', $encode = true)
{
    if (WP_DEBUG && in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
        if ($encode) {
            $text = htmlspecialchars(addslashes($text));
        }

        add_action('wp_footer', function() use ($text, $type) {
            echo <<<JS
<script>
    if (console) {
        console.log('$text');
    }
</script>
JS;
        });
    }
}

/**
 * @param array $custom_query_args
 * @param string $template_slug
 * @param string $template_name
 * 
 * @example:
 * <?php
 *   $custom_archive_args = array(
 *     'post_type' => 'hahu-szalonauto',
 *     'posts_per_page' => 8,
 *     'orderby' => 'post_title',
 *     'order' => 'ASC',
 *   );
 *   cc_custom_archive($custom_archive_args, 'template-parts/archive', 'hahu');
 * ?>
 * 
 */
function cc_custom_archive($custom_query_args, $template_slug, $template_name = '')
{
    $custom_query_args['paged'] = get_query_var('paged') ? get_query_var('paged') : 1;

    $custom_query = new WP_Query($custom_query_args);

    //echo "Last SQL-Query: {$custom_query->request}";

    if ($custom_query->have_posts()) :
        while ($custom_query->have_posts()) :
            $custom_query->the_post();
            echo '<div class="col-sm-6 col-md-4 col-lg-3 mb-5">';
            get_template_part($template_slug, $template_name);
            echo '</div>';
        endwhile;
    endif;

    // Reset postdata
    wp_reset_postdata();

    // Custom query loop pagination
    echo '<div class="col-sm-12 text-center">' . cc_paginate_links($custom_query) . '</div>';
}

/**
 * Saját pagination, mert a wp csak a globális $wp_query változóból tud generálni.
 * Ha már egy ilyen funkció úgyis kellett, ide lehet tenni a pagination customizálását is, ha kell.
 * 
 * @global WP_Query $wp_query
 * @param WP_Query $query
 * @return string
 */
function cc_paginate_links($query = null)
{
    global $wp_query;

    if ($query !== null) {
        $tmp_query = $wp_query;
        $wp_query = $query;
    }

    $content = cc_bootstrap_pagination();

    if ($query !== null) {
        $wp_query = $tmp_query;
    }

    return $content;
}

/**
 * Bootstrap pagination
 * 
 * @global WP_Query $wp_query
 * @return string
 * 
 * source: http://www.ordinarycoder.com/paginate_links-class-ul-li-bootstrap/
 */
function cc_bootstrap_pagination()
{
    global $wp_query;

    $big = 999999999; // need an unlikely integer

    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'type' => 'array',
        'prev_next' => true,
        'prev_text' => __('« Előző'),
        'next_text' => __('Következő »'),
    )
    );

    if (is_array($pages)) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        $pagination = '<ul class="pagination">';
        foreach ($pages as $page) {
            $pagination .= "<li>$page</li>";
        }
        $pagination .= '</ul>';
        return $pagination;
    }
}

/**
 * Excerpt funkció mert a get_the_excerpt nem működik a loopon kívül.
 * Komolyan, ki szerint volt ez jó ötlet? Hogy lehet, hogy tizensok év 
 * fejlesztés után ilyen kibaszott aknák vannak mindenfele.
 * 
 * @global type $post
 * @param type $post_id
 * @return string
 */
function cc_excerpt_by_id($post_id)
{
    global $post;
    $save_post = $post;
    $post = get_post($post_id);
    $excerpt = $post->post_excerpt;
    if (!empty($excerpt)) {
        return $excerpt . ' [...]';
    }
    setup_postdata($post);
    $output = get_the_excerpt();
    $post = $save_post;
    wp_reset_postdata();
    return $output;
}

function cc_relative_date($post_date)
{
    return human_time_diff( $post_date , current_time('timestamp'));
}

