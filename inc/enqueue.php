<?php

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

/**
 * Enqueue theme styles and scripts
 */
add_action('wp_enqueue_scripts', function() {
    // global CSS
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.css');
    wp_enqueue_style('owl', get_stylesheet_directory_uri() . '/assets/owl/assets/owl.carousel.css');
    wp_enqueue_style('owl-theme', get_stylesheet_directory_uri() . '/assets/owl/assets/owl.theme.default.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css');    
    wp_enqueue_style('print', get_template_directory_uri() . '/css/print.css');

    //global JS
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.js');
    wp_enqueue_script('modernizr-custom', get_template_directory_uri() . '/assets/modernizr-custom.js');
    wp_enqueue_script('owl', get_template_directory_uri() . '/assets/owl/owl.carousel.js');
    wp_enqueue_script('site', get_template_directory_uri() . '/js/site.js', array('jquery'));
    wp_enqueue_script('ads-dummy', get_template_directory_uri() . '/js/ads.min.js');
});

/**
 * Enqueue admin styles and scripts
 */
add_action('admin_enqueue_scripts', function() {
    // CSS
    wp_enqueue_style('style-admin-default', get_template_directory_uri() . '/css/editor-style.css');
    
    //JS
    wp_enqueue_script('script-admin-default', get_template_directory_uri() . '/js/admin.js', array('jquery'));
});
