<?php

/*
 * ACF
 */
add_filter('acf/settings/path', function ($path) {
    $path = get_stylesheet_directory() . '/inc/plugins/acf-pro/';
    return $path;
});

add_filter('acf/settings/dir', function ($dir) {
    $dir = get_stylesheet_directory_uri() . '/inc/plugins/acf-pro/';
    return $dir;
});

// Uncomment this if you want to disable acf in admin
// add_filter('acf/settings/show_admin', '__return_false');

include_once( get_stylesheet_directory() . '/inc/plugins/acf-pro/acf.php' );

/*
 * Bootstrap navwalker
 */
// bs3
//require_once get_stylesheet_directory() . '/inc/plugins/wp_bootstrap_navwalker.php';
// bs4
require_once get_stylesheet_directory() . '/inc/plugins/bs4navwalker.php';

/*
 * Kirki
 */
//require_once get_stylesheet_directory() . '/inc/plugins/kirki/kirki.php';

/*
 * Tgmpa
 */
require_once( get_stylesheet_directory() . '/inc/plugins/tgm/class-tgm-plugin-activation.php' );

function cc_require_plugins()
{

    $plugins = array(
        array(
            'name' => 'Clean Filenames',
            'slug' => 'sanitize-spanish-filenames',
            'required' => true,
        ),
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => true,
        ),
        array(
            'name' => 'Flamingo',
            'slug' => 'flamingo',
            'required' => true,
        ),
        array(
            'name' => 'Advanced Custom Fields: Image Aspect Ratio Crop',
            'slug' => 'acf-image-aspect-ratio-crop',
            'required' => true,
        ),
        array(
            'name' => 'WP Migrate DB',
            'slug' => 'wp-migrate-db',
            'required' => false,
        ),
        array(
            'name' => 'Intuitive Custom Post Order',
            'slug' => 'intuitive-custom-post-order',
            'required' => true,
        ),
        array(
            'name' => 'Admin Columns',
            'slug' => 'codepress-admin-columns',
            'required' => false,
        ),
        array(
            'name' => 'Page Builder by SiteOrigin',
            'slug' => 'siteorigin-panels',
            'required' => true,
        ),
        array(
            'name' => 'SiteOrigin Widgets Bundle',
            'slug' => 'so-widgets-bundle',
            'required' => true,
        ),
        array(
            'name' => 'Safe SVG',
            'slug' => 'safe-svg',
            'required' => true,
        ),
        array(
            'name' => 'Easy WP SMTP',
            'slug' => 'easy-wp-smtp',
            'required' => false,
        ),
        array(
            'name' => 'Attachment Pages Redirect',
            'slug' => 'attachment-pages-redirect',
            'required' => false,
        ),
    );
    $config = array(
        'id' => 'cc-sylvania-results-tgmpa', // your unique TGMPA ID
        'default_path' => get_stylesheet_directory() . '/inc/plugins/', // default absolute path
        'menu' => 'cc-install-required-plugins', // menu slug
        'has_notices' => true, // Show admin notices
        'dismissable' => true, // the notices are NOT dismissable
        'dismiss_msg' => 'I really, really need you to install these plugins, okay?', // this message will be output at top of nag
        'is_automatic' => true, // automatically activate plugins after installation
        'message' => '<!--Hey there.-->', // message to output right before the plugins table
        'strings' => array() // The array of message strings that TGM Plugin Activation uses
    );

    tgmpa($plugins, $config);
}
add_action('tgmpa_register', 'cc_require_plugins');