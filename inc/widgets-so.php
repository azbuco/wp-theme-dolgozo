<?php

/*
 * SiteOrigin widgets
 */

// add custom widget folder
add_filter('siteorigin_widgets_widget_folders', function($folders) {
    $folders[] = dirname(__FILE__) . '/widgets-so/';
    return $folders;
});

// create widget group 
add_filter('siteorigin_panels_widget_dialog_tabs', function($tabs) {
    // add cc widgets
    $tabs['cc-widgets-so'] = array(
        'title' => __('CC Widgets', 'cc-widgets-so'),
        'filter' => array(
            'groups' => array('cc-widgets-so')
        )
    );

    // the "all widgets" has a numeric key (0) in the tabs array, and it messes with the order
    // so remove it and keep for later use
    $all = $tabs[0];
    unset($tabs[0]);

    // reorder tabs
    uasort($tabs, function ($a, $b) {
        if ($a == $b) {
            return 0;
        }
        return $a['title'] < $b['title'] ? -1 : 1;
    });

    // move recommended to the beginning
    $tabs = array('recommended' => $tabs['recommended']) + $tabs;

    // add all to the end
    $tabs['all'] = $all;

    return $tabs;
}, 90);

// force activate custom widgets
add_filter('siteorigin_widgets_active_widgets', function ($widgets) {

    $widgets_path = wp_normalize_path(dirname(__FILE__) . '/widgets-so/');
    $widget_folders = glob($widgets_path . '*', GLOB_ONLYDIR);

    foreach ($widget_folders as $path) {
        $name = basename($path);
        $widgets[$name] = true;
    }

    return $widgets;
});

// filter unwanted widgets
add_filter('siteorigin_panels_widgets', function($widgets) {
    $filtered = array();

    foreach ($widgets as $key => $widget) {
        //var_dump($key);
        if (preg_match('/SiteOrigin_|CC_|WP_/', $key)) {
            $filtered[$key] = $widget;
        }
    }

    return $filtered;
}, 90);

// set recommended widgets
add_filter('siteorigin_panels_widgets', function($widgets) {

    $screen = get_current_screen();

    foreach ($widgets as $key => $widget) {
        if ($screen->post_type === 'modell_jelolt') {
            if (in_array($key, array('CC_Image_Block', 'CC_Text_Block'))) {
                $widgets[$key]['groups'][] = 'recommended';
            }
            continue;
        }

        if (preg_match('/SiteOrigin_|CC_/', $key) && !in_array($key, array('CC_Image_Block', 'CC_Text_Block'))) {
            $widgets[$key]['groups'][] = 'recommended';
        }
    }

    return $widgets;
}, 92);

// add vertical align to rows
function cc_inactive_row_style_fields($fields)
{
    $fields['inactive'] = array(
        'name' => __('Vertical center', 'cc'),
        'type' => 'checkbox',
        'group' => 'attributes',
        'description' => __('Center columns in row vertically.', 'cc'),
        'priority' => 1,
    );

    return $fields;
}
add_filter('siteorigin_panels_row_style_fields', 'cc_inactive_row_style_fields');

function cc_custom_row_style_attributes($attributes, $args)
{
    if (!empty($args['inactive']) && $args['inactive'] === true) {
        $attributes['style'] = 'display: flex; align-items: center;';
    }
    
    return $attributes;
}
add_filter('siteorigin_panels_row_style_attributes', 'cc_custom_row_style_attributes', 10, 2);
