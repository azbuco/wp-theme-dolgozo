<?php
$link = isset($instance['link']) ? sow_esc_url($instance['link']) : '';
$open_tag = !empty($link) ? 'a href="' . $link . '"' : 'div';
$close_tag = !empty($link) ? 'a' : 'div';
$background_image = isset($instance['background_image']) ? $instance['background_image'] : false;
if ($background_image) {
    $size = empty( $instance['background_image_size'] ) ? 'large' : $instance['background_image_size'];
    $background_image = wp_get_attachment_image_url($background_image, $size);
}
$background_image_position = isset($instance['background_image_position']) ? $instance['background_image_position'] : 'center center';
$background_video = isset($instance['background_video']) ? $instance['background_video'] : false;
if ($background_video) {
    $background_image = false;
    $background_video = wp_get_attachment_url($background_video);
}
$title = isset($instance['title']) ? $instance['title'] : '';
$subtitle = isset($instance['subtitle']) ? $instance['subtitle'] : '';
$lead = isset($instance['lead']) ? $instance['lead'] : '';
$align = isset($instance['align']) ? $instance['align'] : 'bottom';
$text_align = isset($instance['text_align']) ? ('text-' . $instance['text_align']) : '';
$uppercase = isset($instance['uppercase']) ? ($instance['uppercase'] ? 'text-uppercase' : '') : '';
$font_size = isset($instance['font_size']) ? ($instance['font_size'] ? $instance['font_size'] : 'default') : 'default';
$theme = isset($instance['theme']) ? $instance['theme'] : 'light';
$overlay = isset($instance['overlay']) ? $instance['overlay'] : 0;

$id = uniqid("cc-hero-");
$class = [
    'cc-hero',
    'cc-hero-theme-' . $theme,
    'cc-hero-align-' . $align,
    $text_align,
    $uppercase,
];

$style = 'background: ';
if ($overlay > 0) {
    if ($theme === 'light') {
        $style .= 'linear-gradient(rgba(0,0,0,' . ($overlay/100) . '), rgba(0,0,0,' . ($overlay/100) . ')),';
    } else {
        $style .= 'linear-gradient(rgba(0,0,0,' . ($overlay/100) . '), rgba(0,0,0,' . ($overlay/100) . ')),';
    }
}
if ($background_image) {
    $style .= 'url(' . $background_image .');';
}
$style .= 'background-size: cover; background-position: ' . $background_image_position . ';';

$body = ''
 . '<div class="cc-hero-body font-size-' . $font_size . '">'
 . (!empty($title) ? '<h2 class="title"> ' . $title . '</h2>' : '')
 . (!empty($subtitle) ? '<h3 class="title">' . $subtitle . '</h3>' : '')
 . (!empty(trim($lead)) ? '<div class="cc-hero-lead">' . $lead . '</div>' : '')
 . '</div>';
?>

<<?php echo $open_tag ?> id="<?php echo $id ?>" class="<?php echo implode(' ', $class) ?>">
    <?php if ($background_image): ?>
        <div class="cc-hero-image" style="<?php echo $style ?>">
            <?php if ($align === 'center') echo $body; ?>
        </div>
        <?php if ($align !== 'center') echo $body; ?>
    <?php elseif ($background_video): ?>
        <div class="cc-hero-image">
            <video class="cc-hero-video" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                <source src="<?php echo $background_video ?>" type="video/mp4">
            </video>
        </div>
        <?php echo $body ?>
    <?php else: ?>
        <div class="cc-hero-image">
            <?php echo $body ?>
        </div>
    <?php endif; ?>
</<?php echo $close_tag ?>>

