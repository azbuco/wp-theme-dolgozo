<?php

/*
  Widget Name: Hero
  Description: Hero widget (háttérkép + felirat) a kezdőoldalra.
  Author: sztamas@realszisztema.hu
 */

class CC_Hero extends SiteOrigin_Widget {

    public $textdomain = 'cc-widgets-so';

    function __construct()
    {
        $name = 'Hero';

        $widget_options = array(
            'description' => 'Hero',
            'panels_icon' => 'dashicons dashicons-screenoptions dashicon-digi',
            'panels_groups' => array($this->textdomain),
        );

        //The $control_options array, which is passed through to WP_Widget
        $control_options = array();

        parent::__construct('cc-hero', $name, $widget_options, $control_options, array(), plugin_dir_path(__FILE__));
    }

    function get_widget_form()
    {
        return array(
            'link' => array(
                'type' => 'link',
                'label' => 'Link',
                'default' => '',
            ),
            'background_image' => array(
                'type' => 'media',
                'label' => 'Háttérkép kiválasztása',
                'choose' => 'Kép kiválasztása',
                'update' => 'Kép beállítása',
                'library' => 'image',
                'fallback' => true
            ),
            'background_image_position' => array(
                'type' => 'select',
                'label' => 'Háttérkép pozícionálása',
                'default' => 'center center',
                'options' => array(
                    'left top' => 'Bal felül',
                    'left center' => 'Bal közép',
                    'left bottom' => 'Bal alul',
                    'right top' => 'Jobb felül',
                    'right center' => 'Jobb közép',
                    'right bottom' => 'Jobb alul',
                    'center top' => 'Középen felül',
                    'center center' => 'Középen középen',
                    'center bottom' => 'Középen alul',
                ),
                'description' => 'A háttérkép "cover" módban van beállítva, ezért a pozícionálás erősen korlátozott',
            ),
            'background_image_size' => array(
                'type' => 'image-size',
                'label' => 'Háttérkép méret',
            ),
            'background_video' => array(
                'type' => 'media',
                'label' => 'Háttérvideó kiválasztása',
                'choose' => 'Video kiválasztása',
                'update' => 'Video beállítása',
                'library' => 'video',
                'fallback' => true,
                'description' => 'A háttérben futó video mérete nagyon befolyásolja az oldal sebességét. Csak videofile szúrható be, online videomegosztóról való link nem fog működni'
            ),
            'title' => array(
                'type' => 'text',
                'label' => 'Címsor',
            ),
            'subtitle' => array(
                'type' => 'text',
                'label' => 'Alcím'
            ),
            'lead' => array(
                'type' => 'textarea',
                'label' => 'Szöveg'
            ),
//            'size' => array(
//                'type' => 'select',
//                'label' => 'Magasság',
//                'default' => 'normal',
//                'options' => array(
//                    'sm' => 'Alacsony',
//                    'default' => 'Normál',
//                    'lg' => 'Magas'
//                ),
//            ),
            'align' => array(
                'type' => 'select',
                'label' => 'Szövegblokk igazítása',
                'default' => 'bottom',
                'options' => array(
                    'top' => 'Felül',
                    'bottom' => 'Alul',
                    'center' => 'Középen',
                ),
            ),
            'text_align' => array(
                'type' => 'select',
                'label' => 'Szöveg igazítása',
                'default' => 'center',
                'options' => array(
                    'left' => 'Balra',
                    'right' => 'Jobbra',
                    'center' => 'Középre',
                ),
            ),
            'uppercase' => array(
                'type' => 'checkbox',
                'label' => 'Konvertálás nagybetűre',
                'default' => true,
            ),
            'font_size' => array(
                'type' => 'select',
                'label' => 'Felirat mérete',
                'default' => 'default',
                'options' => array(
                    'default' => 'Normál',
                    'xl' => 'Nagy',
                ),
            ),
            'theme' => array(
                'type' => 'select',
                'label' => 'Téma',
                'default' => 'light',
                'options' => array(
                    'light' => 'Világos',
                    'dark' => 'Sötét',
                ),
            ),
            'overlay' => array(
                'type' => 'slider',
                'label' => 'Háttér tompítása',
                'default' => 20,
                'min' => 0,
                'max' => 100,
                'integer' => true,
            )
        );
    }

    function initialize()
    {
        add_action('siteorigin_widgets_before_widget_cc-hero', array($this, 'enqueue_widget_scripts'));
    }

    public function enqueue_widget_scripts($instance)
    {
        wp_enqueue_style('cc-hero', get_template_directory_uri() . '/inc/widgets-so/cc-hero/css/hero.css');
    }

}

siteorigin_widget_register('cc-hero', __FILE__, 'CC_Hero');
