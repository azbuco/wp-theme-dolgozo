<?php

/*
 * Replace excerpt default [...]
 */
add_filter('the_excerpt', function ($text) {
    return str_replace(' [&hellip;]', '...', $text);
    //return str_replace('[...]', '<br /><a href="' . get_permalink() . '">Read More &rarr;</a>', $text);
});

add_filter('excerpt_length', function($length) {
    return 25;
}, 999);

/*
 * Url field in comments not visible
 */
add_filter('comment_form_default_fields', function($fields) {
    unset($fields['url']);
    return $fields;
});

function cc_wpcf7_mail_default_style($contact_form)
{
    $style = <<<CSS
    
<style>
    .cc-mail-body {
        padding: 15px;
        background: #fff;
        color: #000;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        font-size: 13px;
    }
</style>
    
CSS
    ;

    $mail = $contact_form->prop('mail'); // returns array 
    $mail['body'] = $style . '<div class="cc-mail-body">' . $mail['body'] . '</div>';
    $contact_form->set_properties(array('mail' => $mail));
}

add_action('wpcf7_before_send_mail', 'cc_wpcf7_mail_default_style', 99);

function cc_password_form()
{
    global $post;
    $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
    $o = '<form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
	<h5 class="mt-0">Ez az oldal jelszóval védett</h5>
    <div class="form-group" style="max-width: 300px">
	<label class="pass-label" for="' . $label . '">Jelszó megadása</label><input name="post_password" id="' . $label . '" type="password" class="form-control" />
    </div>
    <input type="submit" name="Submit" class="btn btn-brand" value="Belépés" /> 
	</form>
	';
    return $o;
}

add_filter('the_password_form', 'cc_password_form');
