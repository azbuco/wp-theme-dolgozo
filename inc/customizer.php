<?php

/**
 * Theme customization options
 */
function cc_customize_register($wp_customize)
{
    // normal customizer settings
    $wp_customize->remove_section('static_front_page');
}

add_action('customize_register', 'cc_customize_register');
