<?php

/*
 * Standard WP widgets
 */

$files = glob(dirname(__FILE__) . '/widgets-wp/*');

if (!$files) {
    $files = array();
}

foreach ($files as $file) {
    require_once $file;
}

add_action('widgets_init', function() {
    register_sidebar(array(
        'name' => 'Oldalsáv',
        'id' => 'sidebar-default',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title title">',
        'after_title' => '</h3>',
    ));
    
    register_sidebar(array(
        'name' => 'Lábléc',
        'id' => 'sidebar-footer',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title title">',
        'after_title' => '</h3>',
    ));
});



