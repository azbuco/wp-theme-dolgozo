<?php

/*
 * Theme support
 */
//add_theme_support('custom-background');
//add_theme_support('custom-header');
add_theme_support('custom-logo', array(
//    'height' => 65,
//    'width' => 220,
    'flex-height' => true,
    'flex-width' => true,
));
add_theme_support('customize-selective-refresh-widgets');
add_theme_support('post-formats', array(
//        'aside',
//        'image',
//        'video',
//        'quote',
//        'link',
//        'gallery',
//        'status',
//        'audio',
//        'chat',
));
add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
));
add_theme_support('post-thumbnails');
add_theme_support('title-tag');
add_theme_support('responsive-embeds');

/*
 * Post type support
 * @see https://codex.wordpress.org/Function_Reference/post_type_supports
 */
add_post_type_support('page', 'excerpt');           // enable excerpt for pages
add_post_type_support('post', 'page-attributes');   // enable templates for posts


