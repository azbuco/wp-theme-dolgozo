<?php
cc_debug('Template file: ' . __FILE__);
get_header();
?>

<div class="content-area">
    <div class="container">

        <div class="row">
            <div class="col-md-9">
                <h1 class="main-title mb-5">Keresés: <?php echo get_search_query() ?></h1> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <main id="main" role="main" class="mb-5">
                    <?php if (have_posts()): ?>
                        <?php while (have_posts()): the_post(); ?> 
                            <div class="mb-5">
                                <div class="font-weight-bold"><?php the_title(); ?></div>
                                <div class=""><?php echo cc_excerpt_by_id($post->ID); ?></div>
                                <div class=""><a href="<?php the_permalink(); ?>"><?php the_permalink(); ?></a></div>
                            </div>
                        <?php endwhile ?>
                    <?php else: ?>
                        Sajnáljuk, de a keresésre nincs találat.
                    <?php endif ?>
                </main>
            </div>
            <div class="col-md-3 pl-md-5">
                <aside>
                    <?php if (is_active_sidebar('sidebar-default')) : ?>
                        <?php dynamic_sidebar('sidebar-default'); ?>
                    <?php endif; ?>
                </aside>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();

