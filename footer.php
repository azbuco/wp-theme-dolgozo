
<footer class="page-footer inverse mt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="footer-items d-md-flex justify-content-between">
                    <?php if (is_active_sidebar('sidebar-footer')) : ?>
                        <?php dynamic_sidebar('sidebar-footer'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <?= bloginfo() ?> | <?php echo date('Y') ?>
        </div>
    </div>

</footer>
</div><!-- /#page -->

<?php wp_footer(); ?>

</body>
</html>
