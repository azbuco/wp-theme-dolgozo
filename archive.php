<?php
cc_debug('Template file: ' . __FILE__);
get_header();
?>

<div class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <?php the_archive_title('<h1 class="main-title">', '</h1>'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <main id="main" role="main" class="mb-5">
                    <?php if (have_posts()): ?>
                        <div class="row">

                            <?php while (have_posts()) : the_post(); ?> 
                                <div class="col-md-6 col-lg-4 pb-5">
                                    <?php get_template_part('template-parts/archive', $post->post_type) ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php else: ?>
                        <p>
                            Sajnáljuk, de nincs még bejegyzés. Nézzen vissza kicsit később.
                        </p>
                    <?php endif ?>
                </main>
            </div>
            <div class="col-md-3 pl-md-5">
                <aside>
                    <?php if (is_active_sidebar('sidebar-default')) : ?>
                        <?php dynamic_sidebar('sidebar-default'); ?>
                    <?php endif; ?>
                </aside>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();

