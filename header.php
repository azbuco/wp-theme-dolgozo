<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <?php wp_head(); ?>

    </head>
    <body <?php body_class(); ?>>

        <div id="page">
            <header class="main-header">
                <nav class="navbar navbar-dark bg-brand-primary navbar-expand-lg">
                    <div class="container">
                        <a class="navbar-brand" href="<?php echo home_url(); ?>">
                            <?php
                            $custom_logo_id = get_theme_mod('custom_logo');
                            if ($custom_logo_id):
                                ?>
                                <img src="<?php echo wp_get_attachment_image_src($custom_logo_id, 'full')[0] ?>" alt="logo" />
                            <?php else: ?>
                                <?= get_bloginfo('name') ?>
                            <?php endif ?>
                        </a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-navbar" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div>
                            <div>
                                <form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')) ?>">
                                    <label>
                                        <span class="screen-reader-text"><?php _x('Search for:', 'label') ?></span>
                                        <input type="text" class="search-field" placeholder="<?php echo esc_attr_x('Search', 'placeholder') ?>" value="<?php echo get_search_query() ?>" name="s" />
                                    </label>
                                    <button type="submit" class="search-submit">
                                        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                        <path fill="currentColor" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                                        </svg>
                                    </button>
                                </form>
                            </div>

                            <?php
                            wp_nav_menu(array(
                                'menu' => 'primary',
                                'theme_location' => 'primary',
                                'container' => 'div',
                                'container_id' => 'primary-navbar',
                                'container_class' => 'collapse navbar-collapse',
                                'menu_id' => false,
                                'menu_class' => 'navbar-nav ml-auto',
                                'depth' => 2,
                                'fallback_cb' => 'bs4navwalker::fallback',
                                'walker' => new bs4navwalker()
                            ));
                            ?>
                        </div>

                    </div>
                </nav>
            </header>

