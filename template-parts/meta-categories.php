<?php
$categories = get_the_category();
foreach ($categories as $category):
    $name = $category->name;
    $category_link = get_category_link($category->term_id);
    ?>

    <div class="d-flex align-items-center mr-2 meta-categories">
        <div class="meta-category">
            <a href="<?= $category_link ?>" >
                <span><?= esc_attr($name) ?></span>
            </a>
        </div>
    </div>
    <?php
endforeach;
