<?php
$tags = get_the_tags();
if (is_array($tags)):
    foreach ($tags as $tag):
        $name = $tag->name;
        $tag_link = get_tag_link($tag->term_id);
        ?>

        <div class="d-flex align-items-center mr-2 meta-tags">
            <div class="meta-tag">
                <a href="<?= $tag_link ?>" >
                    <span>#<?= esc_attr($name) ?></span>
                </a>
            </div>
        </div>
        <?php
    endforeach;
endif;
