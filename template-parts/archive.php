<?php
cc_debug('Template file: ' . __FILE__);
?>
<div class="card shadow mb-5 h-100">
    <div class="card-header">
        <h5 class="card-title my-0"><?php the_title() ?></h5>
    </div>
    <div class="card-body flex-grow-1 pb-0">
        <div class="archive-meta mb-3 small text-muted">
            <?php the_time(get_option('date_format') . ' ' . get_option('time_format')) ?>
        </div>
        <div class="archive-content">
            <?php echo cc_excerpt_by_id($post->ID) ?>
        </div>
    </div>
    <div class="card-body flex-grow-0 pt-0 text-center">
        <a href="<?= get_the_permalink() ?>" class="btn btn-brand">Bővebben</a>
    </div>
</div>
