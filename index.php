<?php
cc_debug('Template file: ' . __FILE__);
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <section class="section-singular">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php echo get_template_part('template-parts/content', $post_type) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?php
                    if (is_singular('post')) {
                        cc_the_posts_pagination($post);
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <?php
endwhile;

get_footer();

