<?php

defined('CC_THEME') or define('CC_THEME', 'dolgozo');

// Inc
require_once dirname(__FILE__) . '/inc/supports.php';
require_once dirname(__FILE__) . '/inc/images.php';
require_once dirname(__FILE__) . '/inc/menus.php';
require_once dirname(__FILE__) . '/inc/plugins.php';
require_once dirname(__FILE__) . '/inc/custom-post-types.php';
require_once dirname(__FILE__) . '/inc/custom-fields.php';
require_once dirname(__FILE__) . '/inc/options.php';
require_once dirname(__FILE__) . '/inc/customizer.php';
require_once dirname(__FILE__) . '/inc/hooks-admin.php';
require_once dirname(__FILE__) . '/inc/hooks-frontend.php';
require_once dirname(__FILE__) . '/inc/functions.php';
require_once dirname(__FILE__) . '/inc/gutenberg.php';
require_once dirname(__FILE__) . '/inc/shortcodes.php';
require_once dirname(__FILE__) . '/inc/widgets-so.php';
require_once dirname(__FILE__) . '/inc/widgets-wp.php';
require_once dirname(__FILE__) . '/inc/enqueue.php';
require_once dirname(__FILE__) . '/inc/ajax.php';

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://azbuco@bitbucket.org/azbuco/wp-theme-dolgozo.git',
	__FILE__,
	'wp-theme-dolgozo'
);

//Optional: If you're using a private repository, specify the access token like this:
//$myUpdateChecker->setAuthentication('your-token-here');

//Optional: Set the branch that contains the stable release.
//$myUpdateChecker->setBranch('stable-branch-name');


