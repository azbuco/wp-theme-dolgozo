<?php
cc_debug('Template: ' . __FILE__);
get_header();
?>

<div class="content-area">
    <div class="container">
        <h1>404 - Az oldal nem található</h1>

        <h2 class="mb-5">Sajnáljuk, de a keresett oldal nem található.</h2>
    </div>
</div>

<?php
get_footer();